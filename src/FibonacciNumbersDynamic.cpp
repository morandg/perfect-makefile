/**
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include "FibonacciNumbersDynamic.hpp"

namespace fibo {

//------------------------------------------------------------------------------
FibonacciNumbersDynamic::FibonacciNumbersDynamic()
{
}

//------------------------------------------------------------------------------
FibonacciNumbersDynamic::~FibonacciNumbersDynamic()
{
}

//------------------------------------------------------------------------------
std::string FibonacciNumbersDynamic::getName()
{
  return "dynamic";
}
//------------------------------------------------------------------------------
unsigned int FibonacciNumbersDynamic::getNumber(unsigned int n)
{
  unsigned int f[n+2];
  unsigned int i;

  f[0] = 0;
  f[1] = 1;

  for(i = 2; i <= n; i++)
    f[i] = f[i-1] + f[i-2];

  return f[n];
}

}
