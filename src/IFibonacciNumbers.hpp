/**
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#ifndef _FIBO_I_FIBONACCI_NUMBERS_HPP_
#define _FIBO_I_FIBONACCI_NUMBERS_HPP_

#include <string>
#include <memory>

namespace fibo {

class IFibonacciNumbers
{
public:
  enum Type {
    RECURSIVE   = 0,
    DYNAMIC     = 1,
    DEFAULT     = RECURSIVE
  };

  IFibonacciNumbers();
  virtual ~IFibonacciNumbers();

  static int create(
      std::unique_ptr<IFibonacciNumbers>& fibonacciNumber, Type type);

  virtual std::string getName() = 0;
  virtual unsigned int getNumber(unsigned int n) = 0;
};

}
#endif  // _FIBO_I_FIBONACCI_NUMBERS_HPP_
