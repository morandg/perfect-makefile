/**
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include <getopt.h>

#include <sstream>
#include <iostream>

#include "IFibonacciNumbers.hpp"
#include "Version.hpp"

/* Constants */
static const unsigned int DEFAULT_NUM = 3;
static const fibo::IFibonacciNumbers::Type DEFAULT_TYPE =
    fibo::IFibonacciNumbers::DEFAULT;

/* Local */
unsigned int fiboNumber                 = DEFAULT_NUM;
fibo::IFibonacciNumbers::Type fiboType  = DEFAULT_TYPE;

//------------------------------------------------------------------------------
void showHelp(const char* progName)
{
  std::cout << progName << " -n " << std::endl;
  std::cout << std::endl;
  std::cout << "-h|--help     Show this help" << std::endl;
  std::cout << "-v|--version  Show version" << std::endl;
  std::cout << "-n|--number   Fibonacci number ("
      << DEFAULT_NUM << ")" << std::endl;
  std::cout << "-t|--type     Fibonacci implementation ("
      << (int)DEFAULT_TYPE << ")" << std::endl;
}

//------------------------------------------------------------------------------
void showVersion()
{
  std::cout << fibo::VERSION_MAJOR << "." <<
      fibo::VERSION_MINOR << "." <<
      fibo::VERSION_BUGFIX <<
      " (" << fibo::VERSION_GIT << ")" << std::endl;
}

//------------------------------------------------------------------------------
int parseArgs(int argc, char* argv[])
{
  int opt;
  static struct option long_options[] = {
      {"help",      no_argument,        0, 'h'},
      {"version",   no_argument,        0, 'v'},
      {"number",    required_argument,  0, 'n'},
      {"type",      required_argument,  0, 't'},
      {0,           0,                  0, 0}
  };

  while ((opt = getopt_long(argc, argv, "hvn:t:", long_options, 0)) != -1) {
    std::stringstream ss;

    switch (opt)
    {
    case 'h':
      showHelp(argv[0]);
      return 1;
    case 'v':
      showVersion();
      return 1;
    case 'n':
      ss << optarg;
      ss >> fiboNumber;
      break;
    case 't':
      unsigned int fiboInt;
      ss << optarg;
      ss >> fiboInt;
      fiboType = (fibo::IFibonacciNumbers::Type)fiboInt;
      break;
    default:
      showHelp(argv[0]);
      return -1;
    }
  }

  return 0;
}

//------------------------------------------------------------------------------
int run()
{
  std::unique_ptr<fibo::IFibonacciNumbers> fibo;

  if(fibo::IFibonacciNumbers::create(fibo, fiboType))
  {
    std::cerr << "Unknown implementation " << (int)fiboType << std::endl;
    return -1;
  }

  std::cout << "Computing using implementation " <<
      fibo->getName() << std::endl;
  std::cout << "Fibonacci number of " << fiboNumber << " is ... " <<
    fibo->getNumber(fiboNumber) << std::endl;

  return 0;
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  int retCode;

  retCode = parseArgs(argc, argv);
  if(retCode == 0)
    return run();
  else if(retCode > 0)
    return 0;
  return retCode;
}
